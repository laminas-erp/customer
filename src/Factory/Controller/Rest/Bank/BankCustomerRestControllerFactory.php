<?php

namespace Lerp\Customer\Factory\Controller\Rest\Bank;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Customer\Controller\Rest\Bank\BankCustomerRestController;
use Lerp\Customer\Form\Bank\BankCustomerForm;
use Lerp\Customer\Service\Bank\BankCustomerRelService;

class BankCustomerRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new BankCustomerRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setBankCustomerForm($container->get(BankCustomerForm::class));
        $controller->setBankCustomerRelService($container->get(BankCustomerRelService::class));
        return $controller;
    }
}
