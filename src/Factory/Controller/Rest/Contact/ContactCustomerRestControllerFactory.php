<?php

namespace Lerp\Customer\Factory\Controller\Rest\Contact;

use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Customer\Controller\Rest\Contact\ContactCustomerRestController;
use Lerp\Customer\Form\Contact\ContactCustomerForm;
use Lerp\Customer\Service\Contact\ContactCustomerRelService;

class ContactCustomerRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ContactCustomerRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setContactCustomerForm($container->get(ContactCustomerForm::class));
        $controller->setContactCustomerRelService($container->get(ContactCustomerRelService::class));
        return $controller;
    }
}
