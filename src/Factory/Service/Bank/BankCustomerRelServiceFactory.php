<?php

namespace Lerp\Customer\Factory\Service\Bank;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Customer\Service\Bank\BankCustomerRelService;
use Lerp\Customer\Table\Bank\BankCustomerRelTable;

class BankCustomerRelServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new BankCustomerRelService();
        $service->setLogger($container->get('logger'));
        $service->setBankCustomerRelTable($container->get(BankCustomerRelTable::class));
        return $service;
    }
}
