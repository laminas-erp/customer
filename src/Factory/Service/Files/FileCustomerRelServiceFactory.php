<?php

namespace Lerp\Customer\Factory\Service\Files;

use Bitkorn\Files\Service\FileService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Customer\Service\Files\FileCustomerRelService;
use Lerp\Customer\Table\Files\FileCustomerRelTable;

class FileCustomerRelServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FileCustomerRelService();
        $service->setLogger($container->get('logger'));
        $service->setFileCustomerRelTable($container->get(FileCustomerRelTable::class));
        $service->setFileService($container->get(FileService::class));
        return $service;
    }
}
