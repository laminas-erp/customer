<?php

namespace Lerp\Customer\Factory\Service\Contact;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Customer\Service\Contact\ContactCustomerRelService;
use Lerp\Customer\Table\Contact\ContactCustomerRelTable;

class ContactCustomerRelServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ContactCustomerRelService();
        $service->setLogger($container->get('logger'));
        $service->setContactCustomerRelTable($container->get(ContactCustomerRelTable::class));
        return $service;
    }
}
