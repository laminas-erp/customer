<?php

namespace Lerp\Customer\Factory\Form;

use Bitkorn\Trinket\Table\ToolsTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Lerp\Common\Service\Lists\PayTermService;
use Lerp\Common\Service\Lists\Skr03Service;
use Lerp\Customer\Form\CustomerForm;

class CustomerFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new CustomerForm();
        $form->setDbAdapter($container->get('dbDefault'));
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $form->setLangIsos($toolsTable->getEnumValuesPostgreSQL('enum_supported_lang_iso'));
        /** @var Skr03Service $skr03Service */
        $skr03Service = $container->get(Skr03Service::class);
        $form->setSkr03CodeAssoc($skr03Service->getSkr03CodeAssoc());
        /** @var PayTermService $payTermService */
        $payTermService = $container->get(PayTermService::class);
        $form->setPayTermCodeAssoc($payTermService->getPayTermCodeAssoc());
        return $form;
    }
}
