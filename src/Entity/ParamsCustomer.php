<?php

namespace Lerp\Customer\Entity;

use Bitkorn\Trinket\Entity\ParamsBase;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Filter\DateTimeFormatter;
use Laminas\Filter\Digits;

class ParamsCustomer extends ParamsBase
{
    protected DateTimeFormatter $dateTimeFormatter;
    protected string $dateTimeFormat = 'Y-m-d H:i:s.u';
    protected array $orderFieldsAvailable = ['customer_no', 'customer_label', 'customer_name', 'customer_name2'];
    protected int $customerNo = 0;
    protected string $customerLabel = '';
    protected string $customerName = '';
    protected string $customerName2 = '';
    protected int $countryId = 0;
    protected string $customerEmail = '';
    protected string $customerLangIso = '';
    protected string $industryCategoryUuid = '';
    protected string $timeFrom = '';
    protected string $timeTo = '';

    /**
     * QueryParamsCustomer constructor.
     */
    public function __construct()
    {
        $this->dateTimeFormatter = new DateTimeFormatter();
        $this->dateTimeFormatter->setFormat($this->dateTimeFormat);
    }

    public function setCustomerNo(int $customerNo): void
    {
        $this->customerNo = $customerNo;
    }

    public function setCustomerLabel(string $customerLabel): void
    {
        $this->customerLabel = filter_var($customerLabel, FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
    }

    public function setCustomerName(string $customerName): void
    {
        $this->customerName = filter_var($customerName, FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
    }

    public function setCustomerName2(string $customerName2): void
    {
        $this->customerName2 = filter_var($customerName2, FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
    }

    public function setCountryId(int $countryId): void
    {
        $this->countryId = $countryId;
    }

    public function setCustomerEmail(string $customerEmail): void
    {
        $this->customerEmail = filter_var($customerEmail, FILTER_SANITIZE_SPECIAL_CHARS, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
    }

    public function setCustomerLangIso(string $customerLangIso): void
    {
        $this->customerLangIso = (new Digits())->filter($customerLangIso);
    }

    public function setIndustryCategoryUuid(string $industryCategoryUuid): void
    {
        $this->industryCategoryUuid = $industryCategoryUuid;
    }

    public function setTimeFrom(string $timeFrom): void
    {
        $timeFrom = $this->dateTimeFormatter->filter($timeFrom);
        if ($timeFrom instanceof \DateTime) {
            $timeFrom = $timeFrom->format($this->dateTimeFormat);
        }
        if (!is_string($timeFrom)) {
            return;
        }
        $this->timeFrom = $timeFrom;
    }

    public function setTimeTo(string $timeTo): void
    {
        $timeTo = $this->dateTimeFormatter->filter($timeTo);
        if ($timeTo instanceof \DateTime) {
            $timeTo = $timeTo->format($this->dateTimeFormat);
        }
        if (!is_string($timeTo)) {
            return;
        }
        $this->timeTo = $timeTo;
    }

    public function setFromParamsArray(array $qp): void
    {
        parent::setFromParamsArray($qp);
        $this->setCustomerNo(empty($qp['customer_no']) ? 0 : intval($qp['customer_no']));
        $this->setCustomerLabel($qp['customer_label'] ?? '');
        $this->setCustomerName($qp['customer_name'] ?? '');
        $this->setCustomerName2($qp['customer_name2'] ?? '');
        $this->setCountryId($qp['country_id'] ?? 0);
        $this->setCustomerEmail($qp['customer_email'] ?? '');
        $this->setCustomerLangIso($qp['customer_lang_iso'] ?? '');
        $this->setIndustryCategoryUuid($qp['industry_category_uuid'] ?? '');
        $this->setTimeFrom($qp['time_from'] ?? '');
        $this->setTimeTo($qp['time_to'] ?? '');
    }

    /**
     * @param Select $select If doCount == TRUE then the group function COUNT will produce one result: ['count_customer' => 42]
     * @param string $orderDefault
     */
    public function computeSelect(Select &$select, string $orderDefault = ''): void
    {
        if (!$this->doCount) {
            parent::computeSelect($select);
        } else {
            $select->columns(['count_customer' => new Expression('COUNT(*)')]);
        }
        if (!empty($this->customerNo)) {
            $select->where->like(new Expression('CAST(customer_no AS TEXT)'), '%' . $this->customerNo . '%');
        }
        if (!empty($this->customerLabel)) {
            $text = strtolower($this->customerLabel);
            $select->where->like(new Expression('LOWER(customer_label)'), '%' . $text . '%');
        }
        if (!empty($this->customerName)) {
            $text = strtolower($this->customerName);
            $select->where->like(new Expression('LOWER(customer_name)'), '%' . $text . '%');
        }
        if (!empty($this->customerName2)) {
            $text = strtolower($this->customerName2);
            $select->where->like(new Expression('LOWER(customer_name2)'), '%' . $text . '%');
        }
        if (!empty($this->countryId)) {
            $select->where(['country_id' => $this->countryId]);
        }
        if (!empty($this->customerEmail)) {
            $select->where(['customer_email' => $this->customerEmail]);
        }
        if (!empty($this->customerLangIso)) {
            $select->where(['customer_lang_iso' => $this->customerLangIso]);
        }
        if (!empty($this->industryCategoryUuid)) {
            $select->where(['industry_category_uuid' => $this->industryCategoryUuid]);
        }
        if (!empty($this->timeFrom)) {
            $select->where->greaterThanOrEqualTo('customer_time_create', $this->timeFrom);
        }
        if (!empty($this->timeTo)) {
            $select->where->lessThanOrEqualTo('customer_time_create', $this->timeTo);
        }
    }

}
