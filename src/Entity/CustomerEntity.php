<?php

namespace Lerp\Customer\Entity;

use Bitkorn\Trinket\Entity\AbstractEntity;

class CustomerEntity extends AbstractEntity
{
    public array $mapping = [
        'customer_uuid'          => 'customer_uuid',
        'customer_no'            => 'customer_no',
        'customer_label'         => 'customer_label',
        'customer_name'          => 'customer_name',
        'customer_name2'         => 'customer_name2',
        'customer_street'        => 'customer_street',
        'customer_zip'           => 'customer_zip',
        'customer_city'          => 'customer_city',
        'country_id'             => 'country_id',
        'customer_tel'           => 'customer_tel',
        'customer_fax'           => 'customer_fax',
        'customer_www'           => 'customer_www',
        'customer_email'         => 'customer_email',
        'customer_lang_iso'      => 'customer_lang_iso',
        'customer_tax_id'        => 'customer_tax_id',
        'skr_03_code'            => 'skr_03_code',
        'pay_term_code'          => 'pay_term_code',
        'industry_category_uuid' => 'industry_category_uuid',
        'customer_time_create'   => 'customer_time_create',
        'customer_time_update'   => 'customer_time_update',
        'customer_note'          => 'customer_note',
        'customer_locked'        => 'customer_locked',
    ];

    protected $primaryKey = 'customer_uuid';

    public function getCustomerUuid(): string
    {
        if (!isset($this->storage['customer_uuid'])) {
            return '';
        }
        return $this->storage['customer_uuid'];
    }

    public function getCustomerNo(): int
    {
        if (!isset($this->storage['customer_no'])) {
            return 0;
        }
        return $this->storage['customer_no'];
    }

    public function getCustomerLabel(): string
    {
        if (!isset($this->storage['customer_label'])) {
            return '';
        }
        return $this->storage['customer_label'];
    }

    public function getCustomerName(): string
    {
        if (!isset($this->storage['customer_name'])) {
            return '';
        }
        return $this->storage['customer_name'];
    }

    public function getCustomerName2(): string
    {
        if (!isset($this->storage['customer_name2'])) {
            return '';
        }
        return $this->storage['customer_name2'];
    }

    public function getCustomerStreet(): string
    {
        if (!isset($this->storage['customer_street'])) {
            return '';
        }
        return $this->storage['customer_street'];
    }

    public function getCustomerZip(): string
    {
        if (!isset($this->storage['customer_zip'])) {
            return '';
        }
        return $this->storage['customer_zip'];
    }

    public function getCustomerCity(): string
    {
        if (!isset($this->storage['customer_city'])) {
            return '';
        }
        return $this->storage['customer_city'];
    }

    public function getCountryId(): int
    {
        if (!isset($this->storage['country_id'])) {
            return 0;
        }
        return $this->storage['country_id'];
    }

    public function getCustomerTel(): string
    {
        if (!isset($this->storage['customer_tel'])) {
            return '';
        }
        return $this->storage['customer_tel'];
    }

    public function getCustomerFax(): string
    {
        if (!isset($this->storage['customer_fax'])) {
            return '';
        }
        return $this->storage['customer_fax'];
    }

    public function getCustomerWww(): string
    {
        if (!isset($this->storage['customer_www'])) {
            return '';
        }
        return $this->storage['customer_www'];
    }

    public function getCustomerEmail(): string
    {
        if (!isset($this->storage['customer_email'])) {
            return '';
        }
        return $this->storage['customer_email'];
    }

    public function getCustomerLangIso(): string
    {
        if (!isset($this->storage['customer_lang_iso'])) {
            return '';
        }
        return $this->storage['customer_lang_iso'];
    }

    public function getCustomerTaxId(): string
    {
        if (!isset($this->storage['customer_tax_id'])) {
            return '';
        }
        return $this->storage['customer_tax_id'];
    }

    public function getSkr03Code(): string
    {
        if (!isset($this->storage['skr_03_code'])) {
            return '';
        }
        return $this->storage['skr_03_code'];
    }

    public function getPayTermCode(): int
    {
        if (!isset($this->storage['pay_term_code'])) {
            return 0;
        }
        return $this->storage['pay_term_code'];
    }

    public function getIndustryCategoryUuid(): string
    {
        if (!isset($this->storage['industry_category_uuid'])) {
            return '';
        }
        return $this->storage['industry_category_uuid'];
    }

    public function getCustomerTimeCreate(): string
    {
        if (!isset($this->storage['customer_time_create'])) {
            return '';
        }
        return $this->storage['customer_time_create'];
    }

    public function getCustomerTimeUpdate(): string
    {
        if (!isset($this->storage['customer_time_update'])) {
            return '';
        }
        return $this->storage['customer_time_update'];
    }

    public function getCustomerNote(): string
    {
        if (!isset($this->storage['customer_note'])) {
            return '';
        }
        return $this->storage['customer_note'];
    }

    public function getCustomerLocked(): bool
    {
        if (!isset($this->storage['customer_locked'])) {
            return true;
        }
        return $this->storage['customer_locked'];
    }
}
