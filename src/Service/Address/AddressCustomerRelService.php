<?php

namespace Lerp\Customer\Service\Address;

use Bitkorn\Address\Entity\AddressEntity;
use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Customer\Table\Address\AddressCustomerRelTable;

class AddressCustomerRelService extends AbstractService
{
    protected AddressCustomerRelTable $addressCustomerRelTable;

    public function setAddressCustomerRelTable(AddressCustomerRelTable $addressCustomerRelTable): void
    {
        $this->addressCustomerRelTable = $addressCustomerRelTable;
    }

    public function getAddressCustomerRelJoined(string $addressCustomerRelUuid): array
    {
        return $this->addressCustomerRelTable->getAddressCustomerRelJoined($addressCustomerRelUuid);
    }

    public function insertAddress(AddressEntity $addressEntity, string $supplierUuid): string
    {
        return $this->addressCustomerRelTable->insertAddress($addressEntity, $supplierUuid);
    }

    public function deleteAddress(string $addressCustomerRelUuid): bool
    {
        return $this->addressCustomerRelTable->deleteAddress($addressCustomerRelUuid);
    }

    public function getAddressesForCustomer(string $supplierUuid): array
    {
        return $this->addressCustomerRelTable->getAddressesForCustomer($supplierUuid);
    }

    public function updateAddress(AddressEntity $addressEntity, string $supplierUuid): bool
    {
        return $this->addressCustomerRelTable->updateAddress($addressEntity, $supplierUuid) >= 0;
    }
}
