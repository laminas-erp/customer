<?php

namespace Lerp\Customer\Service\Contact;

use Bitkorn\Contact\Entity\ContactEntity;
use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Customer\Table\Contact\ContactCustomerRelTable;

class ContactCustomerRelService extends AbstractService
{
    protected ContactCustomerRelTable $contactCustomerRelTable;

    public function setContactCustomerRelTable(ContactCustomerRelTable $contactCustomerRelTable): void
    {
        $this->contactCustomerRelTable = $contactCustomerRelTable;
    }

    public function getContactCustomerRelJoined(string $contactCustomerRelUuid): array
    {
        return $this->contactCustomerRelTable->getContactCustomerRelJoined($contactCustomerRelUuid);
    }

    public function insertContact(ContactEntity $contactEntity, string $customerUuid): string
    {
        return $this->contactCustomerRelTable->insertContact($contactEntity, $customerUuid);
    }

    public function deleteContact(string $contactCustomerRelUuid): bool
    {
        return $this->contactCustomerRelTable->deleteContact($contactCustomerRelUuid);
    }

    public function getContactsForCustomer(string $customerUuid): array
    {
        return $this->contactCustomerRelTable->getContactsForCustomer($customerUuid);
    }

    public function updateContact(ContactEntity $contactEntity, string $customerUuid): bool
    {
        return $this->contactCustomerRelTable->updateContact($contactEntity, $customerUuid) >= 0;
    }
}
