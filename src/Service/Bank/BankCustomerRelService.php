<?php

namespace Lerp\Customer\Service\Bank;

use Bitkorn\Bank\Entity\BankEntity;
use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Customer\Table\Bank\BankCustomerRelTable;

class BankCustomerRelService extends AbstractService
{
    protected BankCustomerRelTable $bankCustomerRelTable;

    public function setBankCustomerRelTable(BankCustomerRelTable $bankCustomerRelTable): void
    {
        $this->bankCustomerRelTable = $bankCustomerRelTable;
    }

    public function getBankCustomerRelJoined(string $bankCustomerRelUuid): array
    {
        return $this->bankCustomerRelTable->getBankCustomerRelJoined($bankCustomerRelUuid);
    }

    public function insertBank(BankEntity $bankEntity, string $customerUuid): string
    {
        return $this->bankCustomerRelTable->insertBank($bankEntity, $customerUuid);
    }

    public function deleteBank(string $bankCustomerRelUuid): bool
    {
        return $this->bankCustomerRelTable->deleteBank($bankCustomerRelUuid);
    }

    public function getBanksForCustomer(string $customerUuid): array
    {
        return $this->bankCustomerRelTable->getBanksForCustomer($customerUuid);
    }

    public function updateBank(BankEntity $bankEntity, string $customerUuid): bool
    {
        $bankEntity->unsetDbUnusedFields();
        return $this->bankCustomerRelTable->updateBank($bankEntity, $customerUuid) >= 0;
    }
}
