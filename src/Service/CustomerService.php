<?php

namespace Lerp\Customer\Service;

use Bitkorn\Trinket\Service\AbstractService;
use Lerp\Customer\Entity\CustomerEntity;
use Lerp\Customer\Entity\ParamsCustomer;
use Lerp\Customer\Table\CustomerTable;

class CustomerService extends AbstractService
{
    protected CustomerTable $customerTable;
    protected int $searchCustomerCount = 0;

    public function setCustomerTable(CustomerTable $customerTable): void
    {
        $this->customerTable = $customerTable;
    }

    public function getSearchCustomerCount(): int
    {
        return $this->searchCustomerCount;
    }

    public function insertCustomer(CustomerEntity $customerEntity): string
    {
        return $this->customerTable->insertCustomer($customerEntity);
    }

    public function updateCustomer(CustomerEntity $customerEntity): string
    {
        return $this->customerTable->updateCustomer($customerEntity);
    }

    public function setCustomerLocked(string $customerUuid, bool $locked): bool
    {
        return $this->customerTable->setCustomerLocked($customerUuid, $locked) >= 0;
    }

    public function isCustomerLocked(string $customerUuid): bool
    {
        if(empty($customer = $this->customerTable->getCustomer($customerUuid))) {
            return true;
        }
        return $customer['customer_locked'];
    }

    public function searchCustomer(ParamsCustomer $qpc): array
    {
        $qpc->setDoCount(true);
        $this->searchCustomerCount = $this->customerTable->searchCustomer($qpc)['count_customer'];
        $qpc->setDoCount(false);
        return $this->customerTable->searchCustomer($qpc);
    }

    /**
     * @param string $customerUuid
     * @return array From db.view_customer
     */
    public function getCustomer(string $customerUuid): array
    {
        return $this->customerTable->getCustomer($customerUuid);
    }

    public function deleteCustomer(string $customerUuid): bool
    {
        return $this->customerTable->deleteCustomer($customerUuid) > 0;
    }

    /**
     * @param string $customerNo
     * @return array
     */
    public function getCustomersLikeCustomerNo(string $customerNo): array
    {
        return $this->customerTable->getCustomersLikeCustomerNo($customerNo);
    }
}
