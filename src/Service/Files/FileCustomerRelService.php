<?php

namespace Lerp\Customer\Service\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Service\FileService;
use Bitkorn\Trinket\Service\AbstractService;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Lerp\Customer\Table\Files\FileCustomerRelTable;

class FileCustomerRelService extends AbstractService implements AdapterAwareInterface
{
    protected FileCustomerRelTable $fileCustomerRelTable;
    protected Adapter $adapter;
    protected FileService $fileService;

    public function setFileCustomerRelTable(FileCustomerRelTable $fileCustomerRelTable): void
    {
        $this->fileCustomerRelTable = $fileCustomerRelTable;
    }

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function setFileService(FileService $fileService): void
    {
        $this->fileService = $fileService;
    }

    /**
     * @param string $fileCustomerRelUuid
     * @return array
     */
    public function getFileCustomerRel(string $fileCustomerRelUuid): array
    {
        return $this->fileCustomerRelTable->getFileCustomerRel($fileCustomerRelUuid);
    }

    public function getFileCustomerRelJoined(string $fileCustomerRelUuid): array
    {
        return $this->fileCustomerRelTable->getFileCustomerRelJoined($fileCustomerRelUuid);
    }

    /**
     * @param FileEntity $fileEntity
     * @param string $customerUuid
     * @param string $folderBrand
     * @return string fileCustomerRelUuid
     */
    public function handleFileUpload(FileEntity $fileEntity, string $customerUuid, string $folderBrand = ''): string
    {
        if (empty($fileUuid = $this->fileService->handleFileUpload($fileEntity, $folderBrand))) {
            return '';
        }
        return $this->insertFileCustomerRel($fileUuid, $customerUuid);
    }

    /**
     * @param string $filePath
     * @param string $fileLabel
     * @param string $fileDesc
     * @param string $customerUuid
     * @param string $folderBrand
     * @return string
     */
    public function handleFileCopy(string $filePath, string $fileLabel, string $fileDesc, string $customerUuid, string $folderBrand = ''): string
    {
        if (empty($fileUuid = $this->fileService->handleFileCopy($filePath, $fileLabel, $fileDesc, $folderBrand))) {
            return '';
        }
        return $this->insertFileCustomerRel($fileUuid, $customerUuid);
    }

    protected function insertFileCustomerRel(string $fileUuid, string $customerUuid): string
    {
        if (empty($fileCustomerRelUuid = $this->fileCustomerRelTable->insertFileCustomerRel($fileUuid, $customerUuid))) {
            $this->fileService->deleteFile($fileUuid);
            return '';
        }
        return $fileCustomerRelUuid;
    }

    public function deleteFile(string $fileCustomerRelUuid): bool
    {
        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $fileRel = $this->fileCustomerRelTable->getFileCustomerRel($fileCustomerRelUuid);
        if (
            empty($fileRel) || !isset($fileRel['file_uuid'])
            || $this->fileCustomerRelTable->deleteFile($fileCustomerRelUuid) < 0
            || !$this->fileService->deleteFile($fileRel['file_uuid'])
        ) {
            $connection->rollback();
            return false;
        }
        $connection->commit();
        return true;
    }

    public function getFilesForCustomer(string $customerUuid): array
    {
        return $this->fileCustomerRelTable->getFilesForCustomer($customerUuid);
    }

    public function updateFile(FileEntity $fileEntity, string $customerUuid): bool
    {
        return $this->fileCustomerRelTable->updateFile($fileEntity, $customerUuid) >= 0;
    }
}
