<?php

namespace Lerp\Customer\Form\Contact;

use Bitkorn\Contact\Form\Fieldset\ContactFieldset;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\StringLength;

class ContactCustomerForm extends AbstractForm implements InputFilterProviderInterface
{
    protected ContactFieldset $contactFieldset;

    public function setContactFieldset(ContactFieldset $contactFieldset): void
    {
        $this->contactFieldset = $contactFieldset;
    }

    public function getContactFieldset(): ContactFieldset
    {
        return $this->contactFieldset;
    }

    public function init()
    {
        $this->add($this->contactFieldset);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return $this->contactFieldset->getInputFilterSpecification();
    }
}
