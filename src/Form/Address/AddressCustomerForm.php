<?php

namespace Lerp\Customer\Form\Address;

use Bitkorn\Address\Form\Fieldset\AddressFieldset;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\StringLength;

class AddressCustomerForm extends AbstractForm implements InputFilterProviderInterface
{

    protected AddressFieldset $addressFieldset;

    public function setAddressFieldset(AddressFieldset $addressFieldset): void
    {
        $this->addressFieldset = $addressFieldset;
    }

    public function getAddressFieldset(): AddressFieldset
    {
        return $this->addressFieldset;
    }

    public function init()
    {
        $this->add($this->addressFieldset);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return $this->addressFieldset->getInputFilterSpecification();
    }
}
