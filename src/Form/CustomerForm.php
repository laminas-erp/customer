<?php

namespace Lerp\Customer\Form;

use Bitkorn\Trinket\Filter\FilterChainStringSanitize;
use Bitkorn\Trinket\Form\AbstractForm;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\Digits;
use Laminas\Validator\EmailAddress;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uri;
use Laminas\Validator\Uuid;
use Lerp\Customer\Entity\CustomerEntity;

class CustomerForm extends AbstractForm implements InputFilterProviderInterface, AdapterAwareInterface
{
    protected Adapter $adapter;
    protected array $langIsos;
    protected array $skr03CodeAssoc;
    protected array $payTermCodeAssoc;

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function setLangIsos(array $langIsos): void
    {
        $this->langIsos = $langIsos;
    }

    public function setSkr03CodeAssoc(array $skr03CodeAssoc): void
    {
        $this->skr03CodeAssoc = $skr03CodeAssoc;
    }

    public function setPayTermCodeAssoc(array $payTermCodeAssoc): void
    {
        $this->payTermCodeAssoc = $payTermCodeAssoc;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add(['name' => 'customer_uuid']);
            $this->add(['name' => 'customer_no']);
        }
        $this->add(['name' => 'customer_label']);
        $this->add(['name' => 'customer_name']);
        $this->add(['name' => 'customer_name2']);
        $this->add(['name' => 'customer_street']);
        $this->add(['name' => 'customer_zip']);
        $this->add(['name' => 'customer_city']);
        $this->add(['name' => 'country_id']);
        $this->add(['name' => 'customer_tel']);
        $this->add(['name' => 'customer_fax']);
        $this->add(['name' => 'customer_www']);
        $this->add(['name' => 'customer_email']);
        $this->add(['name' => 'customer_lang_iso']);
        $this->add(['name' => 'customer_tax_id']);
        $this->add(['name' => 'skr_03_code']);
        $this->add(['name' => 'pay_term_code']);
        $this->add(['name' => 'industry_category_uuid']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['customer_uuid'] = [
                'required'      => true,
                'filters'       => [
                    ['name' => FilterChainStringSanitize::class],
                ], 'validators' => [
                    ['name' => Uuid::class]
                ]
            ];
            $filter['customer_no'] = [
                'required'   => true,
                'filters'    => [['name' => FilterChainStringSanitize::class]],
                'validators' => [['name' => Digits::class]]
            ];
        }

        $filter['customer_label'] = [
            'required'   => true,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => StringLength::class, 'options' => ['max' => 80]]]
        ];

        $filter['customer_name'] = [
            'required'   => true,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => StringLength::class, 'options' => ['min' => 1, 'max' => 180]]]
        ];

        $filter['customer_name2'] = [
            'required'   => false,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => StringLength::class, 'options' => ['min' => 1, 'max' => 180]]]
        ];

        $filter['customer_street'] = [
            'required'   => false,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => StringLength::class, 'options' => ['min' => 1, 'max' => 180]]]
        ];

        $filter['customer_zip'] = [
            'required'   => false,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => StringLength::class, 'options' => ['min' => 1, 'max' => 14]]]
        ];

        $filter['customer_city'] = [
            'required'   => false,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => StringLength::class, 'options' => ['min' => 1, 'max' => 180]]]
        ];

        $filter['country_id'] = [
            'required'   => true,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => RecordExists::class, 'options' => ['adapter' => $this->adapter, 'table' => 'country', 'field' => 'country_id']]]
        ];

        $filter['customer_tel'] = [
            'required'   => true,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => StringLength::class, 'options' => ['min' => 1, 'max' => 180]]]
        ];

        $filter['customer_fax'] = [
            'required'   => false,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => StringLength::class, 'options' => ['min' => 1, 'max' => 180]]]
        ];

        $filter['customer_www'] = [
            'required'   => false,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => Uri::class, 'options' => ['allowRelative' => true, 'allowAbsolute' => true]]]
        ];

        $filter['customer_email'] = [
            'required'   => true,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => EmailAddress::class]]
        ];

        $filter['customer_lang_iso'] = [
            'required'   => true,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => InArray::class, 'options' => ['haystack' => $this->langIsos]]]
        ];

        $filter['customer_tax_id'] = [
            'required'   => false,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => StringLength::class, 'options' => ['min' => 1, 'max' => 20]]]
        ];

        $filter['skr_03_code'] = [
            'required'   => true,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => InArray::class, 'options' => ['haystack' => array_keys($this->skr03CodeAssoc)]]]
        ];

        $filter['pay_term_code'] = [
            'required'   => true,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => InArray::class, 'options' => ['haystack' => array_keys($this->payTermCodeAssoc)]]]
        ];

        $filter['industry_category_uuid'] = [
            'required'   => true,
            'filters'    => [['name' => FilterChainStringSanitize::class]],
            'validators' => [['name' => RecordExists::class, 'options' => ['adapter' => $this->adapter, 'table' => 'industry_category', 'field' => 'industry_category_uuid']]]
        ];

        return $filter;
    }

    /**
     * @return CustomerEntity|null
     */
    public function getEntity(): ?CustomerEntity
    {
        $data = $this->getData();
        $entity = new CustomerEntity();
        if (!$entity->exchangeArrayFromDatabase($data)) {
            return null;
        }
        return $entity;
    }
}
