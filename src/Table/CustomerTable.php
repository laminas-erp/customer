<?php

namespace Lerp\Customer\Table;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Lerp\Customer\Entity\CustomerEntity;
use Lerp\Customer\Entity\ParamsCustomer;

class CustomerTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'customer';

    /**
     * @param string $customerUuid
     * @return array From db.view_customer
     */
    public function getCustomer(string $customerUuid): array
    {
        $select = new Select('view_customer');
        try {
            $select->where(['customer_uuid' => $customerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertCustomer(CustomerEntity $customerEntity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'customer_uuid'          => $uuid,
                'customer_label'         => $customerEntity->getCustomerLabel(),
                'customer_name'          => $customerEntity->getCustomerName(),
                'customer_name2'         => $customerEntity->getCustomerName2(),
                'customer_street'        => $customerEntity->getCustomerStreet(),
                'customer_zip'           => $customerEntity->getCustomerZip(),
                'customer_city'          => $customerEntity->getCustomerCity(),
                'country_id'             => $customerEntity->getCountryId(),
                'customer_tel'           => $customerEntity->getCustomerTel(),
                'customer_fax'           => $customerEntity->getCustomerFax(),
                'customer_www'           => $customerEntity->getCustomerWww(),
                'customer_email'         => $customerEntity->getCustomerEmail(),
                'customer_lang_iso'      => $customerEntity->getCustomerLangIso(),
                'customer_tax_id'        => $customerEntity->getCustomerTaxId(),
                'skr_03_code'            => $customerEntity->getSkr03Code(),
                'pay_term_code'          => $customerEntity->getPayTermCode(),
                'industry_category_uuid' => $customerEntity->getIndustryCategoryUuid(),
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateCustomer(CustomerEntity $customerEntity): string
    {
        $update = $this->sql->update();
        if (empty($uuid = $customerEntity->getUuid())) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() CustomerEntity has no primary key.');
        }
        try {
            $update->set([
                'customer_label'         => $customerEntity->getCustomerLabel(),
                'customer_name'          => $customerEntity->getCustomerName(),
                'customer_name2'         => $customerEntity->getCustomerName2(),
                'customer_street'        => $customerEntity->getCustomerStreet(),
                'customer_zip'           => $customerEntity->getCustomerZip(),
                'customer_city'          => $customerEntity->getCustomerCity(),
                'country_id'             => $customerEntity->getCountryId(),
                'customer_tel'           => $customerEntity->getCustomerTel(),
                'customer_fax'           => $customerEntity->getCustomerFax(),
                'customer_www'           => $customerEntity->getCustomerWww(),
                'customer_email'         => $customerEntity->getCustomerEmail(),
                'customer_lang_iso'      => $customerEntity->getCustomerLangIso(),
                'customer_tax_id'        => $customerEntity->getCustomerTaxId(),
                'skr_03_code'            => $customerEntity->getSkr03Code(),
                'pay_term_code'          => $customerEntity->getPayTermCode(),
                'industry_category_uuid' => $customerEntity->getIndustryCategoryUuid(),
                'customer_time_update'   => new Expression('CURRENT_TIMESTAMP'),
            ]);
            $update->where(['customer_uuid' => $uuid]);
            if ($this->updateWith($update) >= 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param ParamsCustomer $qpc
     * @return array
     */
    public function searchCustomer(ParamsCustomer $qpc): array
    {
        $select = $this->sql->select();
        try {
            $qpc->computeSelect($select);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                if (!$qpc->isDoCount()) {
                    return $result->toArray();
                } else {
                    return $result->toArray()[0];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function deleteCustomer(string $customerUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['customer_uuid' => $customerUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param string $customerNo
     * @return array
     */
    public function getCustomersLikeCustomerNo(string $customerNo): array
    {
        $select = $this->sql->select();
        try {
            $select->where->like(new Expression('CAST(customer_no AS TEXT)'), '%' . $customerNo . '%');
            $select->order('customer_no ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function setCustomerLocked(string $customerUuid, bool $locked): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['customer_locked' => $locked]);
            $update->where(['customer_uuid' => $customerUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
