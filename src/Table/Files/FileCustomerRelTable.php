<?php

namespace Lerp\Customer\Table\Files;

use Bitkorn\Files\Entity\FileEntity;
use Bitkorn\Files\Table\AbstractFileTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;

class FileCustomerRelTable extends AbstractFileTable
{
    /** @var string */
    protected $table = 'file_customer_rel';

    /**
     * @param string $fileCustomerRelUuid
     * @return array
     */
    public function getFileCustomerRel(string $fileCustomerRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_customer_rel_uuid' => $fileCustomerRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFileCustomerRel(string $fileCustomerRelUuid): bool
    {
        return !empty($this->getFileCustomerRel($fileCustomerRelUuid));
    }

    /**
     * @param string $fileCustomerRelUuid
     * @return array
     */
    public function getFileCustomerRelJoined(string $fileCustomerRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_customer_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['file_customer_rel_uuid' => $fileCustomerRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getFileCustomerRelForFileAndCustomer(string $fileUuid, string $customerUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['file_uuid' => $fileUuid, 'customer_uuid' => $customerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFileCustomerRelForFileAndCustomer(string $fileUuid, string $customerUuid): bool
    {
        $file = $this->getFileCustomerRelForFileAndCustomer($fileUuid, $customerUuid);
        return !empty($file) && is_array($file);
    }

    public function getFilesForCustomer(string $customerUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('file', 'file.file_uuid = file_customer_rel.file_uuid'
                , $this->filesColumns
                , Select::JOIN_LEFT);
            $select->where(['customer_uuid' => $customerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertFileCustomerRel(string $fileUuid, string $customerUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'file_customer_rel_uuid' => $uuid,
                'file_uuid' => $fileUuid,
                'customer_uuid' => $customerUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function deleteFile(string $fileCustomerRelUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['file_customer_rel_uuid' => $fileCustomerRelUuid]);
            return $this->executeDelete($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateFile(FileEntity $fileEntity, string $customerUuid): int
    {
        $fileUuid = $fileEntity->getFileUuid();
        if (!$this->existFileCustomerRelForFileAndCustomer($fileUuid, $customerUuid)) {
            return -1;
        }
        $fileEntity->unsetPrimaryKey();
        return $this->fileTable->updateFile($fileUuid, $fileEntity);
    }
}
