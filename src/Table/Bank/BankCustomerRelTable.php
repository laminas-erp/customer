<?php

namespace Lerp\Customer\Table\Bank;

use Bitkorn\Bank\Entity\BankEntity;
use Bitkorn\Bank\Table\AbstractBankTable;
use Bitkorn\Bank\Table\BankTable;
use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class BankCustomerRelTable extends AbstractBankTable
{
    /** @var string */
    protected $table = 'bank_customer_rel';

    /**
     * @param string $bankCustomerRelUuid
     * @return array
     */
    public function getBankCustomerRel(string $bankCustomerRelUuid)
    {
        $select = $this->sql->select();
        try {
            $select->where(['bank_customer_rel_uuid' => $bankCustomerRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $bankCustomerRelUuid
     * @return array
     */
    public function getBankCustomerRelJoined(string $bankCustomerRelUuid)
    {
        $select = $this->sql->select();
        try {
            $select->join('bank', 'bank.bank_uuid = bank_customer_rel.bank_uuid'
                , $this->bankColumns
                , Select::JOIN_LEFT);
            $select->where(['bank_customer_rel_uuid' => $bankCustomerRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getBankCustomerRelForBankAndCustomer(string $bankUuid, string $customerUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['bank_uuid' => $bankUuid, 'customer_uuid' => $customerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existBankCustomerRelForBankAndCustomer(string $bankUuid, string $customerUuid): bool
    {
        $bank = $this->getBankCustomerRelForBankAndCustomer($bankUuid, $customerUuid);
        return !empty($bank) && is_array($bank);
    }

    /**
     * @param string $customerUuid
     * @return array
     */
    public function getBanksForCustomer(string $customerUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('bank', 'bank.bank_uuid = bank_customer_rel.bank_uuid'
                , $this->bankColumns
                , Select::JOIN_LEFT);
            $select->where(['customer_uuid' => $customerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    protected function insertBankCustomerRel(string $bankUuid, string $customerUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'bank_customer_rel_uuid' => $uuid,
                'bank_uuid' => $bankUuid,
                'customer_uuid' => $customerUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function insertBank(BankEntity $bankEntity, string $customerUuid): string
    {
        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        if (
            empty($bankUuid = $this->bankTable->insertBank($bankEntity))
            || empty($bankCustomerRelUuid = $this->insertBankCustomerRel($bankUuid, $customerUuid))
        ) {
            $connection->rollback();
            return '';
        }
        $connection->commit();
        return $bankCustomerRelUuid;
    }

    /**
     * @param string $bankCustomerRelUuid
     * @return bool
     */
    public function deleteBank(string $bankCustomerRelUuid): bool
    {
        $bankrel = $this->getBankCustomerRel($bankCustomerRelUuid);
        if (empty($bankrel) || !is_array($bankrel)) {
            return false;
        }

        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $delete = $this->sql->delete();
        try {
            $delete->where(['bank_customer_rel_uuid' => $bankCustomerRelUuid]);
            if ($this->executeDelete($delete) < 1) {
                $connection->rollback();
                return false;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }

        if ($this->bankTable->deleteBank($bankrel['bank_uuid']) < 1) {
            $connection->rollback();
            return false;
        }

        $connection->commit();
        return true;
    }

    public function updateBank(BankEntity $bankEntity, string $customerUuid): int
    {
        $bankUuid = $bankEntity->getBankUuid();
        if (!$this->existBankCustomerRelForBankAndCustomer($bankUuid, $customerUuid)) {
            return -1;
        }
        $bankEntity->unsetPrimaryKey();
        return $this->bankTable->updateBank($bankUuid, $bankEntity);
    }
}
