<?php

namespace Lerp\Customer\Table\Contact;

use Bitkorn\Contact\Entity\ContactEntity;
use Bitkorn\Contact\Table\AbstractContactTable;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ContactCustomerRelTable extends AbstractContactTable
{
    /** @var string */
    protected $table = 'contact_customer_rel';

    /**
     * @param string $contactCustomerRelUuid
     * @return array
     */
    public function getContactCustomerRel(string $contactCustomerRelUuid)
    {
        $select = $this->sql->select();
        try {
            $select->where(['contact_customer_rel_uuid' => $contactCustomerRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $contactCustomerRelUuid
     * @return array
     */
    public function getContactCustomerRelJoined(string $contactCustomerRelUuid)
    {
        $select = $this->sql->select();
        try {
            $select->join('contact', 'contact.contact_uuid = contact_customer_rel.contact_uuid'
                , $this->contactColumns
                , Select::JOIN_LEFT);
            $select->where(['contact_customer_rel_uuid' => $contactCustomerRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getContactCustomerRelForContactAndCustomer(string $contactUuid, string $customerUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['contact_uuid' => $contactUuid, 'customer_uuid' => $customerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existContactCustomerRelForContactAndCustomer(string $contactUuid, string $customerUuid): bool
    {
        $contact = $this->getContactCustomerRelForContactAndCustomer($contactUuid, $customerUuid);
        return !empty($contact) && is_array($contact);
    }

    public function getContactsForCustomer(string $customerUuid)
    {
        $select = $this->sql->select();
        try {
            $select->join('contact', 'contact.contact_uuid = contact_customer_rel.contact_uuid'
                , $this->contactColumns
                , Select::JOIN_LEFT);
            $select->where(['customer_uuid' => $customerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    protected function insertContactCustomerRel(string $contactUuid, string $customerUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'contact_customer_rel_uuid' => $uuid,
                'contact_uuid' => $contactUuid,
                'customer_uuid' => $customerUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function insertContact(ContactEntity $contactEntity, string $customerUuid): string
    {
        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        if (
            empty($contactUuid = $this->contactTable->insertContact($contactEntity))
            || empty($contactCustomerRelUuid = $this->insertContactCustomerRel($contactUuid, $customerUuid))
        ) {
            $connection->rollback();
            return '';
        }
        $connection->commit();
        return $contactCustomerRelUuid;
    }

    public function deleteContact(string $contactCustomerRelUuid): bool
    {
        $contactrel = $this->getContactCustomerRel($contactCustomerRelUuid);
        if (empty($contactrel) || !is_array($contactrel)) {
            return false;
        }

        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $delete = $this->sql->delete();
        try {
            $delete->where(['contact_customer_rel_uuid' => $contactCustomerRelUuid]);
            if ($this->executeDelete($delete) < 1) {
                $connection->rollback();
                return false;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }

        if ($this->contactTable->deleteContact($contactrel['contact_uuid']) < 1) {
            $connection->rollback();
            return false;
        }

        $connection->commit();
        return true;
    }

    public function updateContact(ContactEntity $contactEntity, string $customerUuid): int
    {
        $contactUuid = $contactEntity->getContactUuid();
        if (!$this->existContactCustomerRelForContactAndCustomer($contactUuid, $customerUuid)) {
            return -1;
        }
        $contactEntity->unsetPrimaryKey();
        return $this->contactTable->updateContact($contactUuid, $contactEntity);
    }
}
