<?php

namespace Lerp\Customer\Table\Address;

use Bitkorn\Address\Entity\AddressEntity;
use Bitkorn\Address\Table\AbstractAddressTable;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class AddressCustomerRelTable extends AbstractAddressTable
{
    /** @var string */
    protected $table = 'address_customer_rel';

    public function getAddressCustomerRel(string $addressCustomerRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['address_customer_rel_uuid' => $addressCustomerRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getAddressCustomerRelJoined(string $addressCustomerRelUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('address', 'address.address_uuid = address_customer_rel.address_uuid'
                , $this->addressColumns
                , Select::JOIN_LEFT);
            $select->join('country', 'country.country_id = address.country_id'
                , ['country_name', 'country_iso', 'country_member_eu', 'country_currency_euro']
                , Select::JOIN_LEFT);
            $select->where(['address_customer_rel_uuid' => $addressCustomerRelUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getAddressCustomerRelForAddressAndCustomer(string $addressUuid, string $customerUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['address_uuid' => $addressUuid, 'customer_uuid' => $customerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existAddressCustomerRelForAddressAndCustomer(string $addressUuid, string $customerUuid): bool
    {
        $address = $this->getAddressCustomerRelForAddressAndCustomer($addressUuid, $customerUuid);
        return !empty($address) && is_array($address);
    }

    /**
     * @param string $customerUuid
     * @return array
     */
    public function getAddressesForCustomer(string $customerUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('address', 'address.address_uuid = address_customer_rel.address_uuid'
                , $this->addressColumns
                , Select::JOIN_LEFT);
            $select->join('country', 'country.country_id = address.country_id'
                , ['country_name', 'country_iso', 'country_member_eu', 'country_currency_euro']
                , Select::JOIN_LEFT);
            $select->where(['customer_uuid' => $customerUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    protected function insertAddressCustomerRel(string $addressUuid, string $customerUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'address_customer_rel_uuid' => $uuid,
                'address_uuid' => $addressUuid,
                'customer_uuid' => $customerUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function insertAddress(AddressEntity $addressEntity, string $customerUuid): string
    {
        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        if (
            empty($addressUuid = $this->addressTable->insertAddress($addressEntity))
            || empty($addressCustomerRelUuid = $this->insertAddressCustomerRel($addressUuid, $customerUuid))
        ) {
            $connection->rollback();
            return '';
        }
        $connection->commit();
        return $addressCustomerRelUuid;
    }

    /**
     * @param string $addressCustomerRelUuid
     * @return bool
     */
    public function deleteAddress(string $addressCustomerRelUuid): bool
    {
        $addressrel = $this->getAddressCustomerRel($addressCustomerRelUuid);
        if (empty($addressrel) || !is_array($addressrel)) {
            return false;
        }

        /** @var AbstractConnection $connection */
        $connection = $this->adapter->getDriver()->getConnection();
        $connection->beginTransaction();

        $delete = $this->sql->delete();
        try {
            $delete->where(['address_customer_rel_uuid' => $addressCustomerRelUuid]);
            if ($this->executeDelete($delete) < 1) {
                $connection->rollback();
                return false;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }

        if ($this->addressTable->deleteAddress($addressrel['address_uuid']) < 1) {
            $connection->rollback();
            return false;
        }

        $connection->commit();
        return true;
    }

    public function updateAddress(AddressEntity $addressEntity, string $customerUuid): int
    {
        $addressUuid = $addressEntity->getAddressUuid();
        if(!$this->existAddressCustomerRelForAddressAndCustomer($addressUuid, $customerUuid)) {
            return -1;
        }
        $addressEntity->unsetPrimaryKey();
        return $this->addressTable->updateAddress($addressUuid, $addressEntity);
    }
}
