<?php

namespace Lerp\Customer\Controller\Rest\Contact;

use Bitkorn\Contact\Entity\ContactEntity;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Customer\Form\Contact\ContactCustomerForm;
use Lerp\Customer\Service\Contact\ContactCustomerRelService;

class ContactCustomerRestController extends AbstractUserRestController
{
    protected ContactCustomerForm $contactCustomerForm;
    protected ContactCustomerRelService $contactCustomerRelService;

    public function setContactCustomerForm(ContactCustomerForm $contactCustomerForm): void
    {
        $this->contactCustomerForm = $contactCustomerForm;
    }

    public function setContactCustomerRelService(ContactCustomerRelService $contactCustomerRelService): void
    {
        $this->contactCustomerRelService = $contactCustomerRelService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->contactCustomerForm->getContactFieldset()->setPrimaryKeyAvailable(false)->init();
        $this->contactCustomerForm->setData($data);
        $customerUuid = $data['customer_uuid'];
        $contactEntity = new ContactEntity();
        if (!(new Uuid())->isValid($customerUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->contactCustomerForm->isValid()) {
            $jsonModel->addMessages($this->contactCustomerForm->getMessages());
            return $jsonModel;
        }
        if (!$contactEntity->exchangeArrayFromDatabase($this->contactCustomerForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if (!empty($uuid = $this->contactCustomerRelService->insertContact($contactEntity, $customerUuid))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            $jsonModel->setSuccess(1);
            $jsonModel->setUuid($uuid);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id contact_customer_rel_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->contactCustomerRelService->deleteContact($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $customerUuid = filter_input(INPUT_GET, 'customer_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if ((new Uuid())->isValid($customerUuid) && !empty($customers = $this->contactCustomerRelService->getContactsForCustomer($customerUuid))) {
            $jsonModel->setArr($customers);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $data['customer_uuid'] = $id;
        $this->contactCustomerForm->setData($data);
        $contactEntity = new ContactEntity();
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->contactCustomerForm->isValid()) {
            $jsonModel->addMessages($this->contactCustomerForm->getMessages());
            return $jsonModel;
        }
        if (!$contactEntity->exchangeArrayFromDatabase($this->contactCustomerForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if ($this->contactCustomerRelService->updateContact($contactEntity, $id)) {
            $jsonModel->setSuccess(1);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id contact_customer_rel_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($contact = $this->contactCustomerRelService->getContactCustomerRelJoined($id))) {
            $jsonModel->setObj($contact);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
