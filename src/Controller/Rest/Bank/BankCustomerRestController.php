<?php

namespace Lerp\Customer\Controller\Rest\Bank;

use Bitkorn\Bank\Entity\BankEntity;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Customer\Form\Bank\BankCustomerForm;
use Lerp\Customer\Service\Bank\BankCustomerRelService;

class BankCustomerRestController extends AbstractUserRestController
{
    protected BankCustomerForm $bankCustomerForm;
    protected BankCustomerRelService $bankCustomerRelService;

    public function setBankCustomerForm(BankCustomerForm $bankCustomerForm): void
    {
        $this->bankCustomerForm = $bankCustomerForm;
    }

    public function setBankCustomerRelService(BankCustomerRelService $bankCustomerRelService): void
    {
        $this->bankCustomerRelService = $bankCustomerRelService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->bankCustomerForm->getBankFieldset()->setPrimaryKeyAvailable(false)->init();
        $this->bankCustomerForm->setData($data);
        $customerUuid = $data['customer_uuid'];
        $bankEntity = new BankEntity();
        if (!(new Uuid())->isValid($customerUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->bankCustomerForm->isValid()) {
            $jsonModel->addMessages($this->bankCustomerForm->getMessages());
            return $jsonModel;
        }
        if (!$bankEntity->exchangeArrayFromDatabase($this->bankCustomerForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if (!empty($uuid = $this->bankCustomerRelService->insertBank($bankEntity, $customerUuid))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            $jsonModel->setSuccess(1);
            $jsonModel->setUuid($uuid);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id bank_customer_rel_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->bankCustomerRelService->deleteBank($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $customerUuid = filter_input(INPUT_GET, 'customer_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if ((new Uuid())->isValid($customerUuid) && !empty($customers = $this->bankCustomerRelService->getBanksForCustomer($customerUuid))) {
            $jsonModel->setArr($customers);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id customer_uuid
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $data['customer_uuid'] = $id;
        $this->bankCustomerForm->setData($data);
        $bankEntity = new BankEntity();
        if (!$this->bankCustomerForm->isValid()) {
            $jsonModel->addMessages($this->bankCustomerForm->getMessages());
            return $jsonModel;
        }
        if (!$bankEntity->exchangeArrayFromDatabase($this->bankCustomerForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if ($this->bankCustomerRelService->updateBank($bankEntity, $id)) {
            $jsonModel->setSuccess(1);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id bank_customer_rel_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($address = $this->bankCustomerRelService->getBankCustomerRelJoined($id))) {
            $jsonModel->setObj($address);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
