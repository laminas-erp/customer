<?php

namespace Lerp\Customer\Controller\Rest;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Validator\Uuid;
use Lerp\Customer\Entity\ParamsCustomer;
use Lerp\Customer\Form\CustomerForm;
use Lerp\Customer\Service\CustomerService;

class CustomerController extends AbstractUserRestController
{
    protected CustomerForm $customerForm;
    protected CustomerService $customerService;

    public function setCustomerForm(CustomerForm $customerForm): void
    {
        $this->customerForm = $customerForm;
    }

    public function setCustomerService(CustomerService $customerService): void
    {
        $this->customerService = $customerService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->customerForm->setData($data);
        if (!$this->customerForm->isValid()) {
            $jsonModel->addMessages($this->customerForm->getMessages());
            return $jsonModel;
        }
        if (empty($newUuid = $this->customerService->insertCustomer($this->customerForm->getEntity()))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        $jsonModel->setSuccess(1);
        $jsonModel->setUuid($newUuid);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->customerForm->setPrimaryKeyAvailable(true);
        $this->customerForm->setData(ArrayUtils::merge($data, ['customer_uuid' => $id]));
        if (!$this->customerForm->isValid()) {
            $jsonModel->addMessages($this->customerForm->getMessages());
            return $jsonModel;
        }
        if (empty($uuid = $this->customerService->updateCustomer($this->customerForm->getEntity()))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        $jsonModel->setSuccess(1);
        $jsonModel->setUuid($uuid);
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $qpc = new ParamsCustomer();
        $qpc->setFromParamsArray($this->params()->fromQuery());
        $customers = $this->customerService->searchCustomer($qpc);
        $jsonModel->setArr($customers);
        $jsonModel->setCount($this->customerService->getSearchCustomerCount());
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setObj($this->customerService->getCustomer($id));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->customerService->deleteCustomer($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
