<?php

namespace Lerp\Customer\Controller\Rest\Address;

use Bitkorn\Address\Entity\AddressEntity;
use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserRestController;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;
use Lerp\Customer\Form\Address\AddressCustomerForm;
use Lerp\Customer\Service\Address\AddressCustomerRelService;

class AddressCustomerRestController extends AbstractUserRestController
{
    protected AddressCustomerRelService $addressCustomerRelService;
    protected AddressCustomerForm $addressCustomerForm;

    public function setAddressCustomerRelService(AddressCustomerRelService $addressCustomerRelService): void
    {
        $this->addressCustomerRelService = $addressCustomerRelService;
    }

    public function setAddressCustomerForm(AddressCustomerForm $addressCustomerForm): void
    {
        $this->addressCustomerForm = $addressCustomerForm;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->addressCustomerForm->getAddressFieldset()->setPrimaryKeyAvailable(false)->init();
        $this->addressCustomerForm->setData($data);
        $customerUuid = $data['customer_uuid'];
        $addressEntity = new AddressEntity();
        if (!(new Uuid())->isValid($customerUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!$this->addressCustomerForm->isValid()) {
            $jsonModel->addMessages($this->addressCustomerForm->getMessages());
            return $jsonModel;
        }
        if (!$addressEntity->exchangeArrayFromDatabase($this->addressCustomerForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if (!empty($uuid = $this->addressCustomerRelService->insertAddress($addressEntity, $customerUuid))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            $jsonModel->setSuccess(1);
            $jsonModel->setUuid($uuid);
        }
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id address_customer_rel_uuid
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->addressCustomerRelService->deleteAddress($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $customerUuid = filter_input(INPUT_GET, 'customer_uuid', FILTER_UNSAFE_RAW, ['flags' => [FILTER_FLAG_STRIP_LOW, FILTER_FLAG_STRIP_HIGH]]);
        if ((new Uuid())->isValid($customerUuid) && !empty($customers = $this->addressCustomerRelService->getAddressesForCustomer($customerUuid))) {
            $jsonModel->setArr($customers);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id address_customer_rel_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (!empty($address = $this->addressCustomerRelService->getAddressCustomerRelJoined($id))) {
            $jsonModel->setObj($address);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id customer_uuid
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $data['customer_uuid'] = $id;
        $this->addressCustomerForm->setData($data);
        $addressEntity = new AddressEntity();
        if (!$this->addressCustomerForm->isValid()) {
            $jsonModel->addMessages($this->addressCustomerForm->getMessages());
            return $jsonModel;
        }
        if (!$addressEntity->exchangeArrayFromDatabase($this->addressCustomerForm->getData())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            return $jsonModel;
        }
        if ($this->addressCustomerRelService->updateAddress($addressEntity, $id)) {
            $jsonModel->setSuccess(1);
        } else {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
        }
        return $jsonModel;
    }
}
