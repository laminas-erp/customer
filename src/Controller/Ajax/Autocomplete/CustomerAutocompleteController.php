<?php

namespace Lerp\Customer\Controller\Ajax\Autocomplete;

use Bitkorn\Trinket\View\Model\JsonModel;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Lerp\Customer\Service\CustomerService;

class CustomerAutocompleteController extends AbstractUserController
{
    protected CustomerService $customerService;

    public function setCustomerService(CustomerService $customerService): void
    {
        $this->customerService = $customerService;
    }

    /**
     * @return JsonModel
     */
    public function customerNumberAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->checkUserRoleAccessMin(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $customerNo = $this->params('customer_no');
        if (empty($customerNo)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $customers = $this->customerService->getCustomersLikeCustomerNo($customerNo);
        if (empty($customers)) {
            return $jsonModel;
        }
        $termResult = [];
        foreach ($customers as $customer) {
            $obj = new \stdClass();
            $obj->label = $customer['customer_no'] . ' - ' . $customer['customer_name'];
            $obj->value = $customer['customer_no'];
            $termResult[] = $obj;
        }
        $jsonModel->setArr($termResult);
        return $jsonModel;
    }
}
