<?php

namespace Lerp\Customer;

use Laminas\Router\Http\Segment;
use Lerp\Customer\Controller\Ajax\Autocomplete\CustomerAutocompleteController;
use Lerp\Customer\Controller\Rest\Address\AddressCustomerRestController;
use Lerp\Customer\Controller\Rest\Bank\BankCustomerRestController;
use Lerp\Customer\Controller\Rest\Contact\ContactCustomerRestController;
use Lerp\Customer\Controller\Rest\CustomerController;
use Lerp\Customer\Controller\Rest\Files\FileCustomerRestController;
use Lerp\Customer\Controller\Stream\FilestreamController;
use Lerp\Customer\Factory\Controller\Ajax\Autocomplete\CustomerAutocompleteControllerFactory;
use Lerp\Customer\Factory\Controller\Rest\Address\AddressCustomerRestControllerFactory;
use Lerp\Customer\Factory\Controller\Rest\Bank\BankCustomerRestControllerFactory;
use Lerp\Customer\Factory\Controller\Rest\Contact\ContactCustomerRestControllerFactory;
use Lerp\Customer\Factory\Controller\Rest\CustomerControllerFactory;
use Lerp\Customer\Factory\Controller\Rest\Files\FileCustomerRestControllerFactory;
use Lerp\Customer\Factory\Controller\Stream\FilestreamControllerFactory;
use Lerp\Customer\Factory\Form\Address\AddressCustomerFormFactory;
use Lerp\Customer\Factory\Form\Bank\BankCustomerFormFactory;
use Lerp\Customer\Factory\Form\Contact\ContactCustomerFormFactory;
use Lerp\Customer\Factory\Form\CustomerFormFactory;
use Lerp\Customer\Factory\Form\Files\FileCustomerFormFactory;
use Lerp\Customer\Factory\Service\Address\AddressCustomerRelServiceFactory;
use Lerp\Customer\Factory\Service\Bank\BankCustomerRelServiceFactory;
use Lerp\Customer\Factory\Service\Contact\ContactCustomerRelServiceFactory;
use Lerp\Customer\Factory\Service\CustomerServiceFactory;
use Lerp\Customer\Factory\Service\Files\FileCustomerRelServiceFactory;
use Lerp\Customer\Factory\Table\Address\AddressCustomerRelTableFactory;
use Lerp\Customer\Factory\Table\Bank\BankCustomerRelTableFactory;
use Lerp\Customer\Factory\Table\Contact\ContactCustomerRelTableFactory;
use Lerp\Customer\Factory\Table\CustomerTableFactory;
use Lerp\Customer\Factory\Table\Files\FileCustomerRelTableFactory;
use Lerp\Customer\Form\Address\AddressCustomerForm;
use Lerp\Customer\Form\Bank\BankCustomerForm;
use Lerp\Customer\Form\Contact\ContactCustomerForm;
use Lerp\Customer\Form\CustomerForm;
use Lerp\Customer\Form\Files\FileCustomerForm;
use Lerp\Customer\Service\Address\AddressCustomerRelService;
use Lerp\Customer\Service\Bank\BankCustomerRelService;
use Lerp\Customer\Service\Contact\ContactCustomerRelService;
use Lerp\Customer\Service\CustomerService;
use Lerp\Customer\Service\Files\FileCustomerRelService;
use Lerp\Customer\Table\Address\AddressCustomerRelTable;
use Lerp\Customer\Table\Bank\BankCustomerRelTable;
use Lerp\Customer\Table\Contact\ContactCustomerRelTable;
use Lerp\Customer\Table\CustomerTable;
use Lerp\Customer\Table\Files\FileCustomerRelTable;

return [
    'router'          => [
        'routes' => [
            'lerp_customer_rest_customer'                    => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-customer-rest-customer[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => CustomerController::class,
                    ],
                ],
            ],
            'lerp_customer_rest_addresscustomer'             => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-address-customer[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => AddressCustomerRestController::class,
                    ],
                ],
            ],
            'lerp_customer_rest_bankcustomer'                => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-bank-customer[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => BankCustomerRestController::class,
                    ],
                ],
            ],
            'lerp_customer_rest_contactcustomer'             => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-contact-customer[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => ContactCustomerRestController::class,
                    ],
                ],
            ],
            'lerp_customer_rest_filecustomer'                => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-file-customer[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FileCustomerRestController::class,
                    ],
                ],
            ],
            /**
             * Stream
             */
            'lerp_customer_stream_filestream_stream'         => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-customer-filestream[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]+',
                    ],
                    'defaults'    => [
                        'controller' => FilestreamController::class,
                        'action'     => 'stream'
                    ],
                ],
            ],
            /**
             * AJAX - autocomplete
             */
            'lerp_customer_ajax_autocomplete_customernumber' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/lerp-customer-ajax-autocomplete-customer-no[/:customer_no]',
                    'constraints' => [
                        'customer_no' => '[0-9]+',
                    ],
                    'defaults'    => [
                        'controller' => CustomerAutocompleteController::class,
                        'action'     => 'customerNumber'
                    ],
                ],
            ],
        ],
    ],
    'controllers'     => [
        'factories'  => [
            CustomerController::class             => CustomerControllerFactory::class,
            AddressCustomerRestController::class  => AddressCustomerRestControllerFactory::class,
            BankCustomerRestController::class     => BankCustomerRestControllerFactory::class,
            ContactCustomerRestController::class  => ContactCustomerRestControllerFactory::class,
            FileCustomerRestController::class     => FileCustomerRestControllerFactory::class,
            FilestreamController::class           => FilestreamControllerFactory::class,
            // AJAX
            CustomerAutocompleteController::class => CustomerAutocompleteControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories'  => [
            // form
            CustomerForm::class              => CustomerFormFactory::class,
            AddressCustomerForm::class       => AddressCustomerFormFactory::class,
            BankCustomerForm::class          => BankCustomerFormFactory::class,
            ContactCustomerForm::class       => ContactCustomerFormFactory::class,
            FileCustomerForm::class          => FileCustomerFormFactory::class,
            // service
            CustomerService::class           => CustomerServiceFactory::class,
            AddressCustomerRelService::class => AddressCustomerRelServiceFactory::class,
            BankCustomerRelService::class    => BankCustomerRelServiceFactory::class,
            ContactCustomerRelService::class => ContactCustomerRelServiceFactory::class,
            FileCustomerRelService::class    => FileCustomerRelServiceFactory::class,
            // table
            CustomerTable::class             => CustomerTableFactory::class,
            AddressCustomerRelTable::class   => AddressCustomerRelTableFactory::class,
            BankCustomerRelTable::class      => BankCustomerRelTableFactory::class,
            ContactCustomerRelTable::class   => ContactCustomerRelTableFactory::class,
            FileCustomerRelTable::class      => FileCustomerRelTableFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helpers'    => [
        'factories'  => [],
        'invokables' => [],
        'aliases'    => [],
    ],
    'view_manager'    => [
        'template_map'        => [
            'template/customerUnlockRequest' => __DIR__ . '/../view/template/customerUnlockRequest.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view'
        ],
        'strategies'          => [
            'ViewJsonStrategy',
        ],
    ],
    'lerp_customer'   => [
        'module_brand' => 'customer',
    ],
];
